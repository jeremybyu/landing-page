'use strict'
var webpack = require('webpack')
var path = require('path')

// PATHS
var PATHS = {
  app: path.resolve(__dirname, 'app/'),
  build: path.resolve(__dirname, 'build/')
}

module.exports = {
  entry: path.resolve(PATHS.app, './core/bootstrap.js'),
  output: {
    path: PATHS.build,
    publicPath: '/build/',
    filename: 'bundle.js'
  },
  module: {
    loaders: [{
      test: /\.css$/,
      loader: 'style!css!'
    }, {
      test: /jquery.js$/,
      loader: 'expose?jQuery' //  Need to expose JQuery for angular and bootstrap
    },
    {
      test: /\.js$/,
      loader: 'ng-annotate!babel?presets[]=es2015',
      exclude: /node_modules|bower_components/
    }, {
      test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'file'
    }, {
      test: /\.(woff|woff2)$/,
      loader: 'url?prefix=font/&limit=5000'
    },
    { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url?limit=10000&mimetype=application/octet-stream'
    }, {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url?limit=10000&mimetype=image/svg+xml'
    }
  ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  ]
}
