# FTN Landing Page

## Description
This is an example landing page for an email campaign as specified by `Code Sample Exercise.docx`.
This is a a fully responsive Single Page Application that displays appropriately from desktop to mobile devices.  Includes Dynamic Form Validation and helpful messages to the user.

## How to Run
Run these commands (need node and npm installed):
1. `git clone https://jeremybyu@bitbucket.org/jeremybyu/landing-page.git`
2. `cd landing-page`
3. `npm install`
4. `npm run start`

These commands will clone the repository, install dependencies and then run the webpack-dev-server to server the files at `http://localhost:8080/app`

## Description of Technology Stack
* Bundler/Task Runner - Webpack
* UI/CSS - Bootstrap
* JavaScript - ES6
* MVC Framework - AngularJS
* Router - Angular-UI-Router

### Developer Notes
* Webpack bundles and processes all the source files in the `app/` directory.  Files are included by using `require` syntax. Upon a build command, One file (approx. 450 kb) is concatenated and minified in the directory `build/` which `index.html` references.

* JQuery needed to be included for responsive navbar design for Bootstrap.

* I follow [StandardJS](http://standardjs.com/) for linting purposes through my editor.
* Unit testing is difficult because most of the logic is Angular's templating language. I think E2E integration test would be the only solution for testing.
