'use strict'
/**
 * @index.js
 * This is the main angular application file. It is here that 'app' is defined
 * as well as all its associated routes.
 *
 */
let appModule = angular.module('app', ['ui.router'])
require('./controllers/formController')

appModule.config(($stateProvider, $urlRouterProvider) => {
  $urlRouterProvider.otherwise('/home') //  Defualt router
  $stateProvider
    .state('home', {
      url: '/home',
      templateUrl: 'partials/home.html',
      controller: 'formController'
    })
    .state('about', {
      url: '/about',
      templateUrl: 'partials/about.html'
    })
    .state('contact', {
      url: '/contact',
      templateUrl: 'partials/contact.html'
    })
})

module.exports = appModule
