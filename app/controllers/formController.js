/**
 * @formController.js
 * This is the controller that handles all buisness logic for the form.
 * Here I would probably use the q service to upload infomation to our servers.
 * Instead I simply log to the developer console.
 * Note: Form Validation logic is all in is Angular DSL (Domain Specific Language) templating
 *
 */
angular
  .module('app')
  .controller('formController', ($scope) => {
    $scope.submitForm = function () {
      // Ensure Form is Valid
      if ($scope.userForm.$valid) {
        //  Log user form output to console.
        console.log($scope.user)
      }
    }
  })
