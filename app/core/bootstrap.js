'use strict'
/**
 * @bootstrap.js
 * This file inlcudes all our css and js (vendor.js) while also
 * bootsrapping angular for the html document
 *
 */

require('./vendor.js')() // Gets all our files (Jquery, bootstrap, css, etc.)
var appModule = require('../index') //  Loads the angular app

angular.element(document).ready(function () {
  angular.bootstrap(document, [appModule.name], {
    // strictDi: true
  })
})

jQuery(document).on('click', '.navbar-collapse.in', function (e) {
  if (jQuery(e.target).is('a')) {
    jQuery(this).collapse('hide')
  }
})
