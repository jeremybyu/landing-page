module.exports = function () {
  /* Styles */
  require('../css/custom.css')
  require('bootstrap/dist/css/bootstrap.css')

  /* JS */
  require('jquery/dist/jquery.js')
  require('angular')
  require('angular-ui-router')
  require('bootstrap/dist/js/bootstrap.js')
}
